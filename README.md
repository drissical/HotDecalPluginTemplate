## HotDecalPluginTemplate
This is a visual studio project template for decal plugins with builtin hot reloading and serialization to disk.  Any time the plugin changes on disk it will automatically be reloaded ingame, while maintaining state. This plugin requires the [Virindi View System](http://virindi.net/wiki/index.php/Virindi_Views) to work properly.

### Setup
1. Download the latest project template zip file from [releases](https://gitlab.com/trevis/HotDecalPluginTemplate/-/releases).
2. Add the zip file to `C:\Users\<username>\Documents\Visual Studio 2019\Templates\ProjectTemplates\`. ie: `C:\Users\<username>\Documents\Visual Studio 2019\Templates\ProjectTemplates\HotDecalPluginTemplate.zip`
3. Start Visual Studio 2019 and create a new project, choose the `Decal Hot-Reloadable Plugin Template` as your project template.
4. This creates two projects, one is the loader, one is the actual plugin.
5. Build the solution and add `bin/Project.dll` to decal. (Note: There are two dlls, `Project.dll` and `ProjectPlugin.dll`.  Make sure you are adding `Project.dll` to decal, this is the loader)
6. On Subsequent builds you should only rebuild the plugin project, not the loader.  This will cause the plugin to automatically be reloaded ingame with the new code. (If you need to rebuild the loader, you will need to shut down your client as it will be locked by decal)

### Additional Info

**Important Note**: When the plugin is reloaded (when you are already ingame) it will miss decal events like Login.  This can be important if your plugin relies on events to setup state.

Most if not all of your edits will take place in the `PluginLogic` class to start with. 

All public fields inside of `PluginLogic` will automatically be serialized/unserialized between sessions.

`HotDecalPluginLoader` is loaded as a network filter, this means it is started right when ac client is started (even before logging in to a character).  It listens for decal's `Core_PluginInitComplete` event to load the actual plugin assembly (this happens as soon as you enter world).  Once in-game, it will monitor the file system for changes to `HotDecalPlugin.dll` and will reload it as required.